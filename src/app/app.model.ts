export interface UserToken {
    accessToken: string;
}

export interface LoginData {
    email: string;
    password: string;
}

export type Brand<T, B extends string> = T & { readonly __name: B };

export interface RecordWrapup {
    wrapupId: number;
    agentId: number;
    wrapupComment: string;
}

export type CallRecord = {
    channel: string;
    contactPhone: string;
    displayedNumber: string;
    duration: number;
    finishedAt: string;
    id: number;
    sessionId: string;
    startedAt: string;
    userId: number;
    wrapups: RecordWrapup[];
    agentId: number;
    wrapupComment: string;
    wrapupId: number;
};

export type CallRecordLite = Pick<CallRecord,
    'id' |
    'startedAt' |
    'finishedAt' |
    'duration' |
    'contactPhone'>;

export const callColumnsNames = [
    'id',
    'startedAt',
    'finishedAt',
    'duration',
    'contactPhone',
];
