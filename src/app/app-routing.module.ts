import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full',
            },
            {
                path: 'login',
                loadChildren: () => import('app/pages/login/login.module').then(m => m.LoginModule),
            },
            {
                path: 'records',
                canLoad: [AuthGuard],
                loadChildren: () => import('app/pages/records/records.module').then(m => m.RecordsModule),
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
