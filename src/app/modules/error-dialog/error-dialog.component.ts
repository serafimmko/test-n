import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-error-dialog',
    template: `
        <div>
            <div>
                <p> {{ error }} </p>
            </div>
        </div>
    `
})
export class ErrorDialogComponent {
    constructor(
        @Inject(MAT_DIALOG_DATA) public error: string,
    ) {
    }
}
