import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorDialogComponent } from './error-dialog.component';
import { ErrorDialogService } from './error-dialog.service';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    declarations: [
        ErrorDialogComponent
    ],
    imports: [
        CommonModule,
        MatDialogModule,
    ],
    providers: [
        ErrorDialogService,
    ],
})
export class ErrorDialogModule {
}
