import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private userService: UserService,
        private router: Router,
    ) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const isLoggedIn = !!this.userService.getToken();

        if (!isLoggedIn) {
            this.router.navigate(['/login']);
        }

        return isLoggedIn;
    }

    canLoad(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const isLoggedIn = !!this.userService.getToken();

        if (!isLoggedIn) {
            this.router.navigate(['/login']);
        }

        return isLoggedIn;
    }

}
