import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm = this.formBuilder.group({
        email: ['smile@me.now', Validators.required],
        password: ['nutedau', Validators.required],
    });
    errors: any = {
        email: null,
        password: null,
    };

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
    }

    login() {
        const data = this.loginForm.getRawValue();
        this.userService.login(data)
            .pipe(
                take(1),
            )
            .subscribe((res) => {
                this.router.navigate(['/records']);
            });
    }
}
