import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { pluck, takeUntil } from 'rxjs/operators';
import { AutoUnsubscribe, AutoUnsubscribeComponent } from '../../../../decorators/auto-unsubscribe.decorator';
import { StoreService } from '../../store/store.service';
import { CallRecord, RecordWrapup } from '../../../../app.model';
import { Actions, ofType } from '@ngrx/effects';
import * as fromRecords from '../../store/record.actions';
import { MatSnackBar } from '@angular/material/snack-bar';

@AutoUnsubscribe()
@Component({
    selector: 'app-record-item',
    templateUrl: './record-item.component.html',
    styleUrls: ['./record-item.component.scss']
})
export class RecordItemComponent implements OnInit, AutoUnsubscribeComponent {
    componentDestroy: any;
    callForm: FormGroup;
    userId: number;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private storeService: StoreService,
        private actions: Actions,
        private matSnackBar: MatSnackBar,
    ) {
    }

    ngOnInit(): void {
        this.activatedRoute.data
            .pipe(
                pluck('result'),
                takeUntil(this.componentDestroy()),
            )
            .subscribe((callRecord: CallRecord) => {
                this.userId = callRecord.userId;
                this.callForm = this.formBuilder.group({
                    id: [{ value: callRecord.id, disabled: true }],
                    startedAt: [{ value: callRecord.startedAt, disabled: true }],
                    finishedAt: [{ value: callRecord.finishedAt, disabled: true }],
                    duration: [{ value: callRecord.duration, disabled: true }],
                    contactPhone: [{ value: callRecord.contactPhone, disabled: true }],
                    displayedNumber: [{ value: callRecord.displayedNumber, disabled: true }],
                    wrapups: this.groupWrapups(callRecord.wrapups),
                });
            });

        this.actions
            .pipe(
                ofType(fromRecords.RecordActionsTypes.UPDATE_RECORD_SUCCESS)
            )
            .subscribe(res => {
                this.matSnackBar.open('Record updated', null, {
                    duration: 1000,
                });
            });
    }

    saveCall() {
        const updatedRecord = {
            ...this.callForm.getRawValue(),
            userId: this.userId,
        };
        this.storeService.updateRecord(updatedRecord);
    }

    trackWrapup(idx, item: FormControl): string {
        return `${ idx }__${ item.value }`;
    }

    private groupWrapups(wrapups: RecordWrapup[]): FormArray {
        const groups = wrapups.map((item: RecordWrapup) => this.formBuilder.group({
            wrapupId: [{ value: item.wrapupId, disabled: true }],
            agentId: [{ value: item.agentId, disabled: true }],
            wrapupComment: [{ value: item.wrapupComment, disabled: false }],
        }));
        return this.formBuilder.array(groups);
    }
}
