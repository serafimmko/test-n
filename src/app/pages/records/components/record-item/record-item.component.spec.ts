import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordItemComponent } from './record-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { instance, mock, when } from 'ts-mockito';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { StoreService } from '../../store/store.service';
import { provideMockActions } from '@ngrx/effects/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Action } from '@ngrx/store';
import { CallRecord } from '../../../../app.model';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('RecordItemComponent', () => {
    let component: RecordItemComponent;
    let fixture: ComponentFixture<RecordItemComponent>;
    const mockActivatedRoute = mock<ActivatedRoute>(ActivatedRoute);
    const mockStoreService = mock<StoreService>(StoreService);
    const actions$: Observable<Action> = of({ type: 'test' });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientTestingModule,
                MatFormFieldModule,
                MatSnackBarModule,
                MatInputModule,
                MatFormFieldModule,
                NoopAnimationsModule,
            ],
            declarations: [
                RecordItemComponent,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useFactory: () => instance(mockActivatedRoute),
                },
                {
                    provide: StoreService,
                    useFactory: () => instance(mockStoreService),
                },
                provideMockActions(() => actions$),
            ]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RecordItemComponent);
        component = fixture.componentInstance;
        when(mockActivatedRoute.data).thenReturn(of({
            result: {
                id: 11,
                startedAt: '',
                finishedAt: '',
                duration: 100,
                contactPhone: '',
                displayedNumber: '',
                wrapups: [],
            } as CallRecord,
        }));
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
