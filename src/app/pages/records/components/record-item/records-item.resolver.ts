import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { CallRecord } from '../../../../app.model';
import { RecordsService } from '../../services/records.service';

@Injectable()
export class RecordsItemResolver {

    constructor(
        private recordsService: RecordsService,
        private router: Router,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CallRecord> {
        return this.recordsService.getActiveRecord(route.params.id);
    }
}
