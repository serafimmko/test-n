import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordsListComponent } from './records-list.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ActivatedRoute } from '@angular/router';
import { instance, mock, when } from 'ts-mockito';
import { UserService } from '../../../../services/user.service';
import { of } from 'rxjs';
import { MatTableModule } from '@angular/material/table';
import { SplitNamePipe } from '../../pipes/split-name.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreService } from '../../store/store.service';
import { CallRecordLite } from '../../../../app.model';

describe('RecordsListComponent', () => {
    let component: RecordsListComponent;
    let fixture: ComponentFixture<RecordsListComponent>;
    const mockActivatedRoute = mock<ActivatedRoute>(ActivatedRoute);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatToolbarModule,
                MatIconModule,
                MatTableModule,
                RouterTestingModule,
            ],
            declarations: [
                RecordsListComponent,
                SplitNamePipe,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useFactory: () => instance(mockActivatedRoute),
                },
                {
                    provide: UserService,
                    useValue: {},
                },
                {
                    provide: StoreService,
                    useValue: {},
                },
            ]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RecordsListComponent);
        component = fixture.componentInstance;
        when(mockActivatedRoute.data).thenReturn(of({
            result: [{
                id: 11,
                startedAt: '',
                finishedAt: '',
                duration: 100,
                contactPhone: '',
            }] as CallRecordLite[],
        }));
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
