import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { pluck, takeUntil } from 'rxjs/operators';
import { AutoUnsubscribe, AutoUnsubscribeComponent } from '../../../../decorators/auto-unsubscribe.decorator';
import { callColumnsNames, CallRecordLite } from '../../../../app.model';
import { StoreService } from '../../store/store.service';

@AutoUnsubscribe()
@Component({
    selector: 'app-records-list',
    templateUrl: './records-list.component.html',
    styleUrls: ['./records-list.component.scss']
})
export class RecordsListComponent implements OnInit, AutoUnsubscribeComponent {
    componentDestroy: any;
    columnNames: string[] = callColumnsNames;
    dataSource: CallRecordLite[];

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private storeService: StoreService,
    ) {
    }

    ngOnInit(): void {
        this.activatedRoute.data
            .pipe(
                pluck('result'),
                takeUntil(this.componentDestroy()),
            )
            .subscribe(res => {
                this.dataSource = res;
            });
    }

    navigate(row: any) {
        this.storeService.setActiveRecord(row.id);
        this.router.navigate([`/records/${ row.id }`]);
    }
}
