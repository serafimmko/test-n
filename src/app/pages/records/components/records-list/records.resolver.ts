import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { CallRecordLite } from '../../../../app.model';
import { RecordsService } from '../../services/records.service';

@Injectable()
export class RecordsResolver {

    constructor(
        private recordsService: RecordsService,
        private router: Router,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CallRecordLite[]> {
        return this.recordsService.getRecordsList();
    }
}
