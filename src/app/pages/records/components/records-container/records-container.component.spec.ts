import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordsContainerComponent } from './records-container.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../../services/user.service';

describe('RecordsContainerComponent', () => {
    let component: RecordsContainerComponent;
    let fixture: ComponentFixture<RecordsContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                MatIconModule,
                MatToolbarModule,
            ],
            declarations: [
                RecordsContainerComponent
            ],
            providers: [
                {
                    provide: UserService,
                    useValue: {},
                }
            ],
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RecordsContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
