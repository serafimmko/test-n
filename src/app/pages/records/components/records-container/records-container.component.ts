import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter, map, takeUntil } from 'rxjs/operators';
import { AutoUnsubscribe, AutoUnsubscribeComponent } from '../../../../decorators/auto-unsubscribe.decorator';

@AutoUnsubscribe()
@Component({
    selector: 'app-records-container',
    templateUrl: './records-container.component.html',
    styleUrls: ['./records-container.component.scss']
})
export class RecordsContainerComponent implements OnInit, AutoUnsubscribeComponent {
    currentPage: string;
    componentDestroy: any;

    constructor(
        private userService: UserService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
        this.currentPage = this.getPage(this.router.url);
        this.router.events
            .pipe(
                filter((event: RouterEvent) => event instanceof NavigationEnd),
                map((event: RouterEvent) => this.getPage(event.url)),
                takeUntil(this.componentDestroy()),
            )
            .subscribe((page: string) => {
                this.currentPage = page;
            });
    }

    logout() {
        this.userService.logOut();
    }

    getPage(url: string): string {
        const splittedUrl = url.split('/');
        return url.match(/\/records\/\d+/g)
            ? `Call ${splittedUrl[2]}`
            : 'Call list';
    }

    back() {
        this.router.navigate(['/records']);
    }
}
