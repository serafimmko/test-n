import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecordsListComponent } from './components/records-list/records-list.component';
import { AuthGuard } from '../../guards/auth.guard';
import { RecordsResolver } from './components/records-list/records.resolver';
import { RecordItemComponent } from './components/record-item/record-item.component';
import { RecordsContainerComponent } from './components/records-container/records-container.component';
import { RecordsItemResolver } from './components/record-item/records-item.resolver';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: RecordsContainerComponent,
        children: [
            {
                path: '',
                resolve: {
                    result: RecordsResolver,
                },
                component: RecordsListComponent,
            },
            {
                path: ':id',
                component: RecordItemComponent,
                resolve: {
                    result: RecordsItemResolver,
                },
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecordsRoutingModule {
}
