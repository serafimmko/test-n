import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import * as fromIndex from './index';
import { switchMap, take, tap } from 'rxjs/operators';
import * as fromRecords from './record.reducer';
import { State } from './record.reducer';
import { ApiService } from '../services/api.service';
import { SetActiveRecord, SetRecordAndActive, SetRecordsAction, UpdateRecord } from './record.actions';
import { CallRecord } from '../../../app.model';

@Injectable()
export class StoreService {

    constructor(
        private apiService: ApiService,
        private store: Store<fromRecords.State>,
    ) {
    }

    getRecords(): Observable<any> {
        return this.store.pipe(select(fromIndex.getRecordsState))
            .pipe(
                take(1),
                switchMap((state: State) => {
                    return (state.records.length === 0)
                        ? this.apiService.getRecordsData().pipe(
                            tap((records: CallRecord[]) => {
                                this.store.dispatch(new SetRecordsAction(records));
                            }),
                        )
                        : of(state.records);
                }),
            );
    }

    getActiveRecord(id: number): Observable<any> {
        return this.store.pipe(select(fromIndex.getRecordsState))
            .pipe(
                take(1),
                switchMap((state: State) => {
                    return (!state.activeRecord)
                        ? this.apiService.getRecordsData()
                            .pipe(
                                tap((records: CallRecord[]) => {
                                    this.store.dispatch(new SetRecordAndActive({ id, records }));
                                }),
                                switchMap((records: CallRecord[]) => {
                                    return of(records.find((record: CallRecord) => record.id === +id));
                                })
                            )
                        : of(state.activeRecord);
                }),
            );
    }

    setActiveRecord(id: number): void {
        this.store.dispatch(new SetActiveRecord({ id }));
    }

    updateRecord(record: CallRecord): void {
        this.store.dispatch(new UpdateRecord(record));
    }

}
