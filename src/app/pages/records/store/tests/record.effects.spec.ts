import { EffectsModule } from '@ngrx/effects';
import { RecordEffects } from '../record.effects';
import { TestBed } from '@angular/core/testing';
import * as fromRecords from '../record.actions';
import * as fromRoot from '../index';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { CallRecord } from '../../../../app.model';
import { cold, hot } from 'jest-marbles';
import { ApiService } from '../../services/api.service';

describe('Record effects', () => {
    let store: MockStore<fromRoot.State>;
    let effects: RecordEffects;
    let actions$: Observable<Action>;

    const initialState: fromRoot.State = {
        recordState: {
            records: [] as any,
            activeRecord: null,
        },
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                EffectsModule.forRoot([
                    RecordEffects,
                ]),
            ],
            providers: [
                provideMockActions(() => actions$),
                provideMockStore({ initialState }),
                {
                    provide: ApiService,
                    useValue: {
                        updateRecord: val => of(val),
                    },
                },
            ]
        });

        effects = TestBed.inject(RecordEffects);
        store = TestBed.get(Store);
    });

    it('should set records', () => {
        const testPayload = [{ id: 11 } as CallRecord];
        const action = new fromRecords.SetRecordsAction(testPayload);
        const outcome = new fromRecords.SetRecordsActionSuccess([{ id: 11 } as CallRecord]);

        actions$ = hot('-a', { a: action });
        const expected = cold('-b', { b: outcome });

        expect(effects.setRecords).toBeObservable(expected);
    });

    it('should update record', () => {
        const testPayload = { id: 11 } as CallRecord;
        const action = new fromRecords.UpdateRecord(testPayload);
        const outcome = new fromRecords.UpdateRecordSuccess({ id: 11 } as CallRecord);

        actions$ = hot('-a', { a: action });
        const expected = cold('-b', { b: outcome });

        expect(effects.updateRecord).toBeObservable(expected);
    });

});
