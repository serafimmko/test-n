import { async, TestBed } from '@angular/core/testing';
import { StoreService } from '../store.service';
import { initialState, State } from '../record.reducer';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { ApiService } from '../../services/api.service';
import * as fromIndex from '../index';
import { instance, mock, reset, when } from 'ts-mockito';
import { of } from 'rxjs';
import { CallRecord } from '../../../../app.model';
import { Store } from '@ngrx/store';
import { SetActiveRecord, SetRecordsAction } from '../record.actions';

describe('StoreService', () => {
    let service: StoreService;
    let state: State;
    let store: MockStore<State>;
    const mockApiService = mock<ApiService>(ApiService);

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: ApiService,
                    useFactory: () => instance(mockApiService),
                },
                StoreService,
                provideMockStore({
                    initialState,
                    selectors: [
                        {
                            selector: fromIndex.getRecordsState,
                            value: {
                                records: [],
                            },
                        },
                        {
                            selector: fromIndex.getActiveRecord,
                            value: {},
                        },
                    ]
                }),
            ]
        });
        state = { ...initialState };
        service = TestBed.inject(StoreService);
        store = TestBed.get(Store);
    });

    afterEach(() => {
        reset(mockApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call api service', async(() => {
        spyOn(store, 'dispatch');
        when(mockApiService.getRecordsData())
            .thenReturn(of([{ id: 11 } as CallRecord]));
        const testAction = new SetRecordsAction([{ id: 11 } as CallRecord]);
        service.getRecords().subscribe((res: any) => {
            expect(res).toEqual([{ id: 11 }]);
            expect(store.dispatch).toHaveBeenCalledWith(testAction);
        });
    }));


    it('should return active record', () => {
        store.overrideSelector(fromIndex.getActiveRecord, {
            id: 11
        } as any);

        service.getActiveRecord(11).subscribe(res => {
            expect(res).toEqual({
                id: 4,
            } as CallRecord);
        });
    });

    it('should set active record', () => {
        spyOn(store, 'dispatch');
        service.setActiveRecord(4);
        const action = new SetActiveRecord({ id: 4 });
        expect(store.dispatch).toHaveBeenCalledWith(action);
    });
});
