import { CallRecord } from '../../../app.model';
import { Action } from '@ngrx/store';

export enum RecordActionsTypes {
    SET_RECORDS = '[Records] Set records',
    SET_RECORDS_SUCCESS = '[Records] Set records success',
    SET_ACTIVE_RECORD = '[Records] Set active record',
    SET_RECORDS_WITH_ACTIVE = '[Records] Set records and active',
    UPDATE_RECORD = '[Records] update record',
    UPDATE_RECORD_SUCCESS = '[Records] update record success',
}

export class SetRecordsAction implements Action {
    readonly type = RecordActionsTypes.SET_RECORDS;
    readonly payload: CallRecord[];

    constructor(payload: CallRecord[]) {
        this.payload = payload;
    }
}

export class SetRecordsActionSuccess implements Action {
    readonly type = RecordActionsTypes.SET_RECORDS_SUCCESS;

    constructor(public readonly payload: CallRecord[]) {
    }
}

export class SetActiveRecord implements Action {
    readonly type = RecordActionsTypes.SET_ACTIVE_RECORD;

    constructor(public readonly payload: { id: number }) {
    }
}

export class SetRecordAndActive implements Action {
    readonly type = RecordActionsTypes.SET_RECORDS_WITH_ACTIVE;

    constructor(public readonly payload: { id: number, records: CallRecord[] }) {
    }
}

export class UpdateRecord implements Action {
    readonly type = RecordActionsTypes.UPDATE_RECORD;

    constructor(public readonly payload: CallRecord) {
    }
}

export class UpdateRecordSuccess implements Action {
    readonly type = RecordActionsTypes.UPDATE_RECORD_SUCCESS;

    constructor(public readonly payload: CallRecord) {
    }

}

export type RecordActions = SetRecordsAction
    | SetRecordsActionSuccess
    | SetActiveRecord
    | SetRecordAndActive
    | UpdateRecord
    | UpdateRecordSuccess;
