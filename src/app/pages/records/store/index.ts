import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRecords from './record.reducer';

export interface State {
    recordState: fromRecords.State;
}

export const reducers: ActionReducerMap<State> = {
    recordState: fromRecords.reducer,
};

export const getRecordsFeatureState = createFeatureSelector<State>('records');

export const getRecordsState = createSelector(getRecordsFeatureState, (state: State) => state.recordState);
export const getActiveRecord = createSelector(getRecordsFeatureState, (state: State) => fromRecords.getActiveRecord);
