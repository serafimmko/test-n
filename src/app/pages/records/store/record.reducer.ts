import { CallRecord } from '../../../app.model';
import { RecordActions, RecordActionsTypes } from './record.actions';

export interface State {
    records: CallRecord[];
    activeRecord: CallRecord;
}

export const initialState: State = {
    records: [],
    activeRecord: null,
};

export function reducer(state = initialState, action: RecordActions): State {
    switch (action.type) {
        case RecordActionsTypes.SET_RECORDS: {
            return state;
        }
        case RecordActionsTypes.SET_RECORDS_SUCCESS: {
            return {
                ...state,
                records: action.payload,
            };
        }
        case RecordActionsTypes.SET_ACTIVE_RECORD: {
            return {
                ...state,
                activeRecord: state.records.find(({ id }) => id === action.payload.id),
            };
        }
        case RecordActionsTypes.SET_RECORDS_WITH_ACTIVE: {
            return {
                ...state,
                records: action.payload.records,
                activeRecord: action.payload.records.find(({ id }) => id === +action.payload.id),
            };
        }
        case RecordActionsTypes.UPDATE_RECORD_SUCCESS: {
            return {
                ...state,
                records: updateRecords(state.records, action.payload),
            };
        }
        default: {
            return state;
        }
    }
}

export const getRecords = (state: State) => state.records;
export const getActiveRecord = (state: State) => state.activeRecord as CallRecord;
function updateRecords(records: CallRecord[], updated: CallRecord) {
    return records.filter((record: CallRecord) => record.id !== updated.id).concat(updated);
}
