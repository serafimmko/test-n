import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import * as fromRecords from './record.actions';
import { SetRecordsActionSuccess } from './record.actions';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CallRecord } from '../../../app.model';
import { ApiService } from '../services/api.service';

@Injectable()
export class RecordEffects {
    @Effect()
    setRecords: Observable<Action> = this.actions$.pipe(
        ofType<fromRecords.SetRecordsAction>(fromRecords.RecordActionsTypes.SET_RECORDS),
        map(action => action.payload),
        map((records: CallRecord[]) => new SetRecordsActionSuccess(records)),
    );

    @Effect()
    updateRecord: Observable<Action> = this.actions$.pipe(
        ofType<fromRecords.UpdateRecord>(fromRecords.RecordActionsTypes.UPDATE_RECORD),
        map(action => action.payload),
        switchMap(record => this.apiService.updateRecord(record)),
        map((record: CallRecord) => new fromRecords.UpdateRecordSuccess(record)),
    );

    constructor(
        private actions$: Actions,
        private apiService: ApiService,
    ) {
    }
}
