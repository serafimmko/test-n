import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecordsRoutingModule } from './records-routing.module';
import { RecordsListComponent } from './components/records-list/records-list.component';
import { RecordsResolver } from './components/records-list/records.resolver';
import { RecordsService } from './services/records.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { UserService } from '../../services/user.service';
import { MatTableModule } from '@angular/material/table';
import { SplitNamePipe } from './pipes/split-name.pipe';
import { RecordItemComponent } from './components/record-item/record-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store';
import { RecordsContainerComponent } from './components/records-container/records-container.component';
import { EffectsModule } from '@ngrx/effects';
import { RecordEffects } from './store/record.effects';
import { ApiService } from './services/api.service';
import { StoreService } from './store/store.service';
import { RecordsItemResolver } from './components/record-item/records-item.resolver';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
    declarations: [RecordsListComponent, SplitNamePipe, RecordItemComponent, RecordsContainerComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RecordsRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        MatTableModule,
        StoreModule.forFeature('records', reducers),
        EffectsModule.forFeature([
            RecordEffects,
        ]),
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
    ],
    providers: [
        RecordsResolver,
        RecordsItemResolver,
        RecordsService,
        UserService,
        ApiService,
        StoreService,
    ]
})
export class RecordsModule {
}
