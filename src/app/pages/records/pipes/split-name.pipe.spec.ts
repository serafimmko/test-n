import { SplitNamePipe } from './split-name.pipe';

describe('SplitNamePipe', () => {
    let pipe: SplitNamePipe;

    beforeEach(() => {
        pipe = new SplitNamePipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('should split name by letter', () => {
        expect(pipe.transform('startedAt')).toEqual('started at');
        expect(pipe.transform('duration')).toEqual('duration');
    });
});
