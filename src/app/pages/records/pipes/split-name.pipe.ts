import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitName'
})
export class SplitNamePipe implements PipeTransform {

  transform(value: string): string {
    return value.split(/(?=[A-Z])/).join(' ').toLowerCase();
  }

}
