import { async, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StoreService } from '../store/store.service';
import { of } from 'rxjs';
import { CallRecordLite } from '../../../app.model';
import { RecordsService } from './records.service';

describe('RecordsService', () => {
    let service: RecordsService;
    const mockRecord: CallRecordLite = {
        id: 11,
        startedAt: '11',
        finishedAt: '11',
        duration: 11,
        contactPhone: '11',
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                {
                    provide: StoreService,
                    useValue: {
                        getRecords: () => of([mockRecord]),
                    },
                },
                RecordsService,
            ],
        });
        service = TestBed.inject(RecordsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return records list', async(() => {
        service.getRecordsList().subscribe(res => {
            expect(res).toEqual([mockRecord]);
        });
    }));
});
