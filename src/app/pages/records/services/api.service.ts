import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvironmentService } from '../../../services/environment.service';
import { Observable } from 'rxjs';
import { CallRecord, CallRecordLite } from '../../../app.model';
import { map } from 'rxjs/operators';

@Injectable()
export class ApiService {

  constructor(
      private httpClient: HttpClient,
      private environmentService: EnvironmentService,
  ) {
  }

  getRecordsData(): Observable<CallRecord[]> {
    const url = `${ this.environmentService.baseUrl }/records`;
    return this.httpClient.get(url) as Observable<CallRecord[]>;
  }

  getRecords(): Observable<CallRecordLite[]> {
    return this.getRecordsData()
        .pipe(
            map((records: CallRecord[]) => records
                .map((record: CallRecord) => ({
                  id: record.id,
                  startedAt: record.startedAt,
                  finishedAt: record.finishedAt,
                  duration: record.duration,
                  contactPhone: record.contactPhone,
                }))),
        ) as Observable<CallRecordLite[]>;
  }

  updateRecord(record: CallRecord): Observable<any> {
      const url = `${this.environmentService.baseUrl}/records/${record.id}`;
      return this.httpClient.put(url, record);
  }

}
