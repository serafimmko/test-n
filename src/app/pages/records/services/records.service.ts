import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CallRecord, CallRecordLite } from '../../../app.model';
import { map } from 'rxjs/operators';
import { StoreService } from '../store/store.service';

@Injectable()
export class RecordsService {

    constructor(
        private storeService: StoreService,
    ) {
    }

    getRecordsList(): Observable<CallRecordLite[]> {
        return this.storeService.getRecords()
            .pipe(
                map((records: CallRecord[]) => records
                    .map((record: CallRecord) => ({
                        id: record.id,
                        startedAt: record.startedAt,
                        finishedAt: record.finishedAt,
                        duration: record.duration,
                        contactPhone: record.contactPhone,
                    }))),
            ) as Observable<CallRecordLite[]>;
    }

    getActiveRecord(id: number): Observable<CallRecord> {
        return this.storeService.getActiveRecord(id) as Observable<CallRecord>;
    }

}
