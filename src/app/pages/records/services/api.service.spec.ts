import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { CallRecord } from '../../../app.model';

describe('ApiService', () => {
    let service: ApiService;
    let httpMock: HttpTestingController;
    const mockRecords: CallRecord[] = [{
        channel: '',
        contactPhone: '',
        displayedNumber: '',
        duration: 100,
        finishedAt: '',
        id: 1001,
        sessionId: '',
        startedAt: '',
        userId: 100,
        wrapups: [],
        agentId: 100,
        wrapupComment: '',
        wrapupId: 100,
    }];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                ApiService,
            ]
        });
        service = TestBed.inject(ApiService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return records data', () => {
        service.getRecords().subscribe(res => {
            expect(res).toEqual(mockRecords);
        });

        httpMock.match(({ body, url, method }: HttpRequest<any>) => {
            return url === 'https://diabolocom-test.herokuapp.com/records' && method === 'GET';
        })[0].flush(mockRecords);
    });

    it('should return lite records', () => {
        const expected = [{
            id: 11,
            startedAt: '',
            finishedAt: '',
            duration: 100,
            contactPhone: '',
        }];

        service.getRecords().subscribe(res => {
            expect(res).toEqual(expected);
        });

        httpMock.match(({ body, url, method }: HttpRequest<any>) => {
            return url === 'https://diabolocom-test.herokuapp.com/records' && method === 'GET';
        })[0].flush(mockRecords);
    });

    it('should handle record update', () => {
        service.updateRecord(mockRecords[0]).subscribe(res => {
            expect(res).toEqual('get');
        });

        httpMock.match(({ body, url, method }: HttpRequest<any>) => {
            expect(body).toEqual(mockRecords[0]);
            return url === 'https://diabolocom-test.herokuapp.com/records/1001' && method === 'PUT';
        })[0].flush('get');
    });
});
