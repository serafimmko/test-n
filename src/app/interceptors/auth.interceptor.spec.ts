import { TestBed } from '@angular/core/testing';

import { AuthInterceptor } from './auth.interceptor';
import { UserService } from '../services/user.service';
import { MatDialogModule } from '@angular/material/dialog';

describe('AuthInterceptor', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            MatDialogModule,
        ],
        providers: [
            AuthInterceptor,
            {
                provide: UserService,
                useValue: {},
            },
        ]
    }));

    it('should be created', () => {
        const interceptor: AuthInterceptor = TestBed.inject(AuthInterceptor);
        expect(interceptor).toBeTruthy();
    });
});
