import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginData, UserToken } from '../app.model';
import { EnvironmentService } from './environment.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

    constructor(
        private httpClient: HttpClient,
        private environmentService: EnvironmentService,
        private router: Router,
    ) {
    }

    login(data: LoginData): Observable<UserToken> {
        const baseUrl = this.environmentService.baseUrl;
        return this.httpClient.post(`${ baseUrl }/login`, data)
            .pipe(
                tap((token: UserToken) => {
                    localStorage.setItem('token', token.accessToken);
                })
            ) as Observable<UserToken>;
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    logOut(): void {
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
    }
}
