import { async, TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

const localStorageMock = (() => {
    let store: any = {};

    return {
        getItem: (key: string) => {
            return store[key] || null;
        },
        setItem: (key: string, value: any) => {
            store[key] = value.toString();
        },
        clear: () => {
            store = {};
        }
    };

})();
Object.defineProperty(window, 'localStorage', {
    value: localStorageMock
});

describe('UserService', () => {
    let service: UserService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
            ],
            providers: [
                UserService,
            ]
        });
        service = TestBed.inject(UserService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeDefined();
    });

    it('should handle login', async(() => {
        service.login({ email: 'test', password: 'test' }).subscribe(res => {
            expect(res).toEqual({ accessToken: 'test' });
        });

        httpMock.match(({ body, url, method }: HttpRequest<any>) => {
            return url === 'https://diabolocom-test.herokuapp.com/login' && method === 'POST';
        })[0].flush({ accessToken: 'test' });
    }));

    it('should return token', () => {
        localStorage.setItem('token', 'test');
        expect(service.getToken()).toEqual('test');
    });
});
