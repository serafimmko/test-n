import { Injectable } from '@angular/core';
import { Brand } from '../app.model';

type BaseUrl = Brand<string, 'baseUrl'>;

@Injectable({
    providedIn: 'root'
})
export class EnvironmentService {
    baseUrl = 'https://diabolocom-test.herokuapp.com' as BaseUrl;

    get url(): BaseUrl {
        return this.baseUrl;
    }
}
