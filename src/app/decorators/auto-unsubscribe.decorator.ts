import { Subject } from 'rxjs';

export interface AutoUnsubscribeComponent {
    componentDestroy: any;
}

export function AutoUnsubscribe() {
    return (constructor: any) => {
        const originalNgOnDestroy = constructor.prototype.ngOnDestroy;

        constructor.prototype.componentDestroy = function() {
            this._takeUntilDestroy$ = this._takeUntilDestroy$ || new Subject();
            return this._takeUntilDestroy$.asObservable();
        };

        constructor.prototype.ngOnDestroy = function() {
            if (this._takeUntilDestroy$) {
                this._takeUntilDestroy$.next(true);
                this._takeUntilDestroy$.complete();
            }
            if (originalNgOnDestroy && typeof originalNgOnDestroy === 'function') {
                originalNgOnDestroy.apply(this, arguments);
            }
        };
    };
}
